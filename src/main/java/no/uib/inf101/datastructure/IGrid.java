package no.uib.inf101.datastructure;

/**
 * Objects in a class implementing IGrid can get and set the element of a GridCell at a given position.
 *
 * @param <T> the type of the elements in the GridCell
 */
public interface IGrid<T> extends GridDimension, GridCellCollection<T> {

  /**
   * Get the element of the cell at the given position.
   *
   * @param pos the position
   * @return the element of the cell
   * @throws IndexOutOfBoundsException if the position is out of bounds
   */
  T get(CellPosition pos);

  /**
   * Set the element of the cell at the given position.
   *
   * @param pos the position
   * @param elem the new element
   * @throws IndexOutOfBoundsException if the position is out of bounds
   */
  void set(CellPosition pos, T elem);

}
